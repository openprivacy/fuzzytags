use criterion::{criterion_group, criterion_main, BenchmarkId, Criterion};
use fuzzytags::{RootSecret};
use rand::rngs::OsRng;
use std::time::Duration;

fn benchmark_generate_tag(c: &mut Criterion) {
    let mut group = c.benchmark_group("generate_tags");
    group.measurement_time(Duration::new(10, 0));
    group.sample_size(1000);
    let mut rng = OsRng::default();
    let secret_key = RootSecret::<24>::generate(&mut rng);
    let public_key = secret_key.tagging_key();
    for p in [5, 10, 15].iter() {
        group.bench_with_input(BenchmarkId::from_parameter(p), p, |b, _gamma| {
            b.iter(|| public_key.generate_tag(&mut rng))
        });
    }
}

fn benchmark_test_tag(c: &mut Criterion) {
    let mut group = c.benchmark_group("test_tags");
    group.measurement_time(Duration::new(10, 0));
    group.sample_size(1000);
    let mut rng = OsRng::default();

    let secret_key = RootSecret::<24>::generate(&mut rng);

    for p in [5, 10, 15, 24].iter() {
        let detection_key = secret_key.extract_detection_key(*p);
        group.bench_with_input(BenchmarkId::from_parameter(p), p, |b, _gamma| {
            let tag = secret_key.tagging_key().generate_tag(&mut rng);
            b.iter(|| detection_key.test_tag(&tag))
        });
    }
}

criterion_group!(benches, benchmark_generate_tag, benchmark_test_tag);
criterion_main!(benches);
