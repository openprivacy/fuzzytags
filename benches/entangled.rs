use criterion::{criterion_group, criterion_main, BenchmarkId, Criterion};
use fuzzytags::{RootSecret, TaggingKey};
use rand::rngs::OsRng;
use std::time::Duration;

fn benchmark_entangled(c: &mut Criterion) {
    let mut group = c.benchmark_group("entangling");
    group.measurement_time(Duration::new(10, 0));
    group.sample_size(10);
    let mut rng = OsRng::default();
    for p in [24].iter() {
        let secret_key_1 = RootSecret::<24>::generate(&mut rng);
        let secret_key_2 = RootSecret::<24>::generate(&mut rng);
        let public_key_1 = secret_key_1.tagging_key();
        let public_key_2 = secret_key_2.tagging_key();
        group.bench_with_input(BenchmarkId::from_parameter(p), p, |b, _gamma| {
            b.iter(|| {
                TaggingKey::generate_entangled_tag(
                    vec![public_key_1.clone(), public_key_2.clone()],
                    &mut rng,
                    *p,
                )
            })
        });
    }
}

criterion_group!(benches, benchmark_entangled);
criterion_main!(benches);
